import { Component, OnInit } from '@angular/core';
import {RestClientService} from "../rest-client.service";
import {Employee} from "./employee";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {

  constructor(private restClient: RestClientService) {
  }

  employeeList = [];
  menu: any;
  employee: Employee;

  ngOnInit() {
    this.employee = {name: '', salary: null, age: null, id: null, lastname:null}
    this.getAllEmployees();
  }
  getAllEmployees() {
    this.restClient.getEmployees().subscribe(res => {
      console.log('employee list', res);
      this.employeeList = res.data;
    });
  }


  saveEmployee() {
    this.restClient.saveEmployee(this.employee).subscribe(res => {
      console.log('server response', res);
      this.getAllEmployees();
    });
  }
  updateEmployee() {
    this.restClient.updateEmployee(this.employee).subscribe(res => {
      console.log('server update response', res);
      this.getAllEmployees();
    });
  }
  // loadForEditEmployee(param: any) {
  //   this.employee = {
  //     name: param.employee_name,
  //     salary: param.employee_salary,
  //     age: param.employee_age,
  //     id: param.id
  //   };
  // }
  deleteEmployee(id: number) {
    this.restClient.deleteEmployee(id).subscribe(res => {
      console.log('employee deleted', res);
    });
  }



}
