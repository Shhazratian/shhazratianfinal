export class Employee{
  id:number;
  name: string;
  lastname: string;
  salary: number;
  age: number;
}
