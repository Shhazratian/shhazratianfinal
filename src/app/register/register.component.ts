import { Component, OnInit } from '@angular/core';
import {RestClientService} from "../rest-client.service";
import {Employee} from "../employee/employee";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  constructor(private restClient: RestClientService) { }
  employeeList = [];
  employee: Employee;

  ngOnInit() {
    this.employee = {name: '', salary: null, age: null, id: null, lastname:null}
  }


  saveEmployee() {
    this.restClient.saveEmployee(this.employee).subscribe(res => {
      console.log('server response', res);
    });
  }
  }

