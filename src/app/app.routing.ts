import {NgModule} from "@angular/core";
import{RouterModule, Routes} from "@angular/router";
import {EmployeeComponent} from "./employee/employee.component";
import {RegisterComponent} from "./register/register.component";


const r: Routes=[
  {path:'',component: EmployeeComponent},
  {path: 'employee', component: EmployeeComponent},
  {path: 'register', component: RegisterComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(r)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}
