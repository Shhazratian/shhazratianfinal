import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Employee} from "./employee/employee";
import {Register} from "ts-node";
import {urlJoin} from "@angular-devkit/build-angular/src/utils";
import {url} from "inspector";

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  constructor(private http: HttpClient) { }
  getEmployees(): Observable<any> {
    return this.http.get('http://dummy.restapiexample.com/api/v1/employees');
  }

  saveEmployee(employee: Employee):Observable<any>{
    return this.http.post('http://dummy.restapiexample.com/api/v1/create', employee);

  }
  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put('http://dummy.restapiexample.com/api/v1/update/' + employee, employee);
  }
  deleteEmployee(id: number): Observable<any> {
    return this.http.delete('http://dummy.restapiexample.com/api/v1/delete/' + id);
  }


}
